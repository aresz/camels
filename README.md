### Requirements
1. node.js: https://nodejs.org/en/
2. Gulp global: `npm install -g gulp`
3. Bower global: `npm install -g bower`

### Initialize
1. `npm install`
2. `bower install`
3. `gulp serve`
