(function() {
  'use strict';

  angular
    .module('camels', ['ngRoute'])
    .config(function($routeProvider){
      $routeProvider
          .when('/', {
            templateUrl: 'app/main/main.html',
            controller: 'MainCtrl'
          })
          .otherwise({
            redirectTo: '/'
          });

    });
})();
